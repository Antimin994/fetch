const root = document.querySelector("#root");

fetch('https://ajax.test-danit.com/api/swapi/films')
  .then((response) => {
    return response.json();
  })
  .then((data) => {
    console.log(data);
    data.forEach((value, index, array) => {
      
      const filmCard = document.createElement('div');
      filmCard.classList.add('card');
      filmCard.innerHTML = `<p>Star Wars  episode ${value.episodeId}: ${value.name}</p>
      <p>Description: ${value.openingCrawl}</p>
      <p>Characters: </p>`
      

      for (let i = 0; i < value.characters.length; i++) {
        fetch(value.characters[i])
        .then((response) => {
          return response.json();
        })
        .then((data) => {
          console.log(data.name);
          filmCard.innerHTML += `| ${data.name} |`
        });
        
      }

      root.appendChild(filmCard);

    })
  });

  document.body.onload = function() {
    setTimeout(function() {
      const loader = document.querySelector(".loader");
      if (!loader.classList.contains('loader_done')) {
        loader.classList.add('loader_done');
      }

    }, 2000)
  }

  console.log(root.innerHTML);

 